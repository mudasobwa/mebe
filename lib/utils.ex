defmodule MebeWeb.Utils do
  alias MebeEngine.Models
  alias MebeEngine.DB

  @moduledoc """
  This module contains functions and other stuff that just don't fit anywhere else properly.
  """

  @doc """
  Get a configuration setting.

  Uses Application.get_env to get the given setting's value. Use this instead of module attributes
  so the configuration settings are not compiled in!
  """
  def get_conf(key) do
    Application.get_env :mebe_web, key
  end

  @doc """
  Get the author of a post.

  Returns a value according to the following pseudocode

  if multi author mode is on then
    if post has author then
      return post's author
    else if use default author is on then
      return blog author
    else return nil
  else if use default author is on then
    return blog author
  else return nil
  """
  def get_author(post = %Models.Post{}) do
    multi_author_mode = get_conf(:multi_author_mode)
    use_default_author = get_conf(:use_default_author)
    blog_author = get_conf(:blog_author)

    if multi_author_mode do
      cond do
        Map.has_key?(post.extra_headers, "author") ->
          Map.get(post.extra_headers, "author")

        use_default_author ->
          blog_author

        true -> nil
      end
    else
      if use_default_author, do: blog_author, else: nil
    end
  end

  @doc """
  Get slug out of a given value.

  Nil is returned as is.
  """
  def slugify(nil), do: nil

  def slugify(value) do
    Slugger.slugify_downcase(value)
  end

  @doc """
  Get the author name related to this slug from the DB.
  """
  def unslugify_author(slug) do
    DB.get_author_name(slug)
  end
end
