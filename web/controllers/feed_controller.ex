defmodule MebeWeb.FeedController do
  use MebeWeb.Web, :controller

  alias MebeEngine.DB
  alias MebeWeb.Utils
  import MebeWeb.ControllerUtils, only: [render_posts: 4, render_404: 1]

  plug :check_feeds_enabled
  plug :put_resp_content_type, "application/rss+xml"
  plug :put_layout_formats, ["xml"]
  plug :put_layout, "feed.xml"

  def index(conn, params) do
    posts = DB.get_reg_posts(0, Utils.get_conf(:posts_in_feed))

    conn
    |> render_posts(posts, "postlist.xml", params)
  end

  def tag(conn, params = %{"tag" => tag}) do
    posts = DB.get_tag_posts(tag, 0, Utils.get_conf(:posts_in_feed))

    conn
    |> assign(:tag, tag)
    |> render_posts(posts, "postlist.xml", params)
  end

  def author(conn, params = %{"author" => author}) do
    posts = DB.get_author_posts(author, 0, Utils.get_conf(:posts_in_feed))

    conn
    |> assign(:author, author)
    |> render_posts(posts, "postlist.xml", params)
  end

  # A private plug to check if feeds are enabled at runtime. We need to do this
  # at runtime to make it work with releases (where the release is compiled with
  # configuration that was active when the release was made). This plug will halt
  # the request processing, returning a 404 error if feeds are not enabled.
  defp check_feeds_enabled(conn, _opts) do
    case Utils.get_conf(:enable_feeds) do
      true -> conn
      false ->
        conn
        |> render_404
        |> halt
    end
  end
end
