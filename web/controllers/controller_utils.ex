defmodule MebeWeb.ControllerUtils do
  use MebeWeb.Web, :controller

  alias MebeWeb.Utils

  @moduledoc """
  This module contains the common functions for all controllers.
  """

  @doc """
  Render a list of posts with the given template and params. The posts
  and generic configuration settings are assigned to the connection.
  """
  def render_posts(conn, posts, template, _params) do
    conn
    |> insert_config
    |> assign(:posts, posts)
    |> render(template)
  end

  @doc """
  Insert common config variables to the assigns table for the connection
  """
  def insert_config(conn) do
    conn
    |> assign(:blog_name, Utils.get_conf(:blog_name))
    |> assign(:blog_author, Utils.get_conf(:blog_author))
    |> assign(:absolute_url, Utils.get_conf(:absolute_url))
    |> assign(:multi_author_mode, Utils.get_conf(:multi_author_mode))
    |> assign(:use_default_author, Utils.get_conf(:use_default_author))
    |> assign(:posts_per_page, Utils.get_conf(:posts_per_page))
    |> assign(:disqus_comments, Utils.get_conf(:disqus_comments))
    |> assign(:page_commenting, Utils.get_conf(:page_commenting))
    |> assign(:disqus_shortname, Utils.get_conf(:disqus_shortname))
    |> assign(:enable_feeds, Utils.get_conf(:enable_feeds))
    |> assign(:force_read_more, Utils.get_conf(:force_read_more))
    |> assign(:use_default_style, Utils.get_conf(:use_default_style))
    |> assign(:custom_style_path, Utils.get_conf(:custom_style_path))
  end

  @doc """
  Render the 404 page.
  """
  def render_404(conn) do
    conn
    |> insert_config
    |> put_status(:not_found)
    |> render(MebeWeb.ErrorView, :"404")
  end
end
